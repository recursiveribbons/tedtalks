FROM gradle:7.6.0-jdk17-alpine as buildstep
WORKDIR /app
COPY . .
RUN gradle bootJar

FROM eclipse-temurin:17-jre-alpine
COPY --from=buildstep /app/build/libs/*.jar /opt/talks/lib/app.jar
ENTRYPOINT ["java"]
CMD ["-jar", "/opt/talks/lib/app.jar"]
