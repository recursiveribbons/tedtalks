# TedTalk API

This is an API that provides information on TED talks

## How to run

### Gradle

`./gradlew bootRun`

### Docker Compose

#### Local build

1. Fill in a proper password inside the [compose file](docker-compose.build.yml)
2. Run `docker compose -f docker-compose.build.yml up --build`

#### Docker registry

1. Fill in a proper password inside the [compose file](docker-compose.yml)
2. Run `docker compose up`

## API documentation

Visit `/swagger-ui.html` to see the Swagger UI, and `/v3/api-docs` for the JSON formatted OpenAPI spec.

## Architecture

The application is built on Spring Data JPA for data repositories, with a Service layer for business logic, and Spring
Boot Web for REST APIs.
The API is documented with Swagger. Integration tests uses H2, while production uses a Postgres Docker container.