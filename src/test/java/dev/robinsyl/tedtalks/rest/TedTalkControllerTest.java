package dev.robinsyl.tedtalks.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import dev.robinsyl.tedtalks.entities.TedTalk;
import dev.robinsyl.tedtalks.services.TedTalkService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(TedTalkController.class)
class TedTalkControllerTest {

    @MockBean
    TedTalkService service;

    MockMvc mvc;
    ObjectMapper mapper;

    TedTalk test1 = new TedTalk(12L, "\"Dive In\"", "Dave Matthews", "October 2021", 1600L, 49L, URI.create("https://ted.com/talks/dave_matthews_dive_in"));
    TedTalk test2 = new TedTalk(17L, "Climate action needs new frontline leadership", "Ozawa Bineshi Albert", "December 2021", 404000L, 12000L, URI.create("https://ted.com/talks/ozawa_bineshi_albert_climate_action_needs_new_frontline_leadership"));

    @Autowired
    public TedTalkControllerTest(MockMvc mvc, ObjectMapper mapper) {
        this.mvc = mvc;
        this.mapper = mapper;
    }

    @BeforeEach
    void setUp() {
        when(service.getTedTalkById(any())).thenReturn(Optional.empty());
        when(service.getTedTalkById(12L)).thenReturn(Optional.of(test1));
        when(service.getTedTalksPaged(eq(0), intThat(i -> i >= 2), eq(true), eq("title"))).thenReturn(List.of(test2, test1));
        when(service.searchTedTalks("\"Dive In\"", null, null, null)).thenReturn(List.of(test1));
        when(service.saveTedTalk(test1)).thenReturn(test1);
        when(service.editTedTalk(12L, test1)).thenReturn(test1);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void testGetSingle() throws Exception {
        mvc.perform(get("/tedtalks/12").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.title", equalTo("\"Dive In\"")))
                .andExpect(jsonPath("$.likes", equalTo(49)));

    }

    @Test
    void testGetPaged() throws Exception {
        mvc.perform(get("/tedtalks")
                        .param("page", "0")
                        .param("pageSize", "10")
                        .param("sortBy", "title")
                        .param("sortDirection", "asc")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()", equalTo(2)))
                .andExpect(jsonPath("$[0].id", equalTo(17)))
                .andExpect(jsonPath("$[1].id", equalTo(12)));
    }

    @Test
    void testSearch() throws Exception {
        mvc.perform(get("/tedtalks/search")
                        .param("author", "\"Dive In\""))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()", equalTo(1)))
                .andExpect(jsonPath("$[0].id", equalTo(12)));
    }

    @Test
    void testCreate() throws Exception {
        mvc.perform(post("/tedtalks")
                        .content(mapper.writeValueAsBytes(test1))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", containsString("12")))
                .andExpect(jsonPath("$.id", equalTo(12)));
    }

    @Test
    void testDelete() throws Exception {
        mvc.perform(delete("/tedtalks/12"))
                .andExpect(status().isNoContent());
        verify(service).deleteTedTalk(12L);
    }

    @Test
    void testPut() throws Exception {
        mvc.perform(put("/tedtalks/12")
                        .content(mapper.writeValueAsBytes(test1))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", equalTo(12)));
        verify(service).editTedTalk(12L, test1);
    }

    @Test
    void testPatch() throws Exception {
        mvc.perform(patch("/tedtalks/12")
                        .content(mapper.writeValueAsBytes(test1))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", equalTo(12)));
        verify(service).editTedTalk(12L, test1);
    }
}