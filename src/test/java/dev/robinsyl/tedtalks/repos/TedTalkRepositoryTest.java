package dev.robinsyl.tedtalks.repos;

import dev.robinsyl.tedtalks.entities.TedTalk;
import jakarta.validation.ConstraintViolationException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.net.URI;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DataJpaTest
class TedTalkRepositoryTest {

    TestEntityManager entityManager;
    TedTalkRepository tedTalkRepository;

    @Autowired
    public TedTalkRepositoryTest(TestEntityManager entityManager, TedTalkRepository tedTalkRepository) {
        this.entityManager = entityManager;
        this.tedTalkRepository = tedTalkRepository;
    }

    @BeforeEach
    void setUp() {
        entityManager.clear();
    }

    @AfterEach
    void tearDown() {
        entityManager.clear();
    }

    @Test
    void canSaveTedTalk() {
        TedTalk talk = new TedTalk("\"Dive In\"", "Dave Matthews", "October 2021", 1600L, 49L, URI.create("https://ted.com/talks/dave_matthews_dive_in"));
        TedTalk savedTalk = tedTalkRepository.save(talk);
        entityManager.flush();

        assertThat(savedTalk.getAuthor(), equalTo("Dave Matthews"));
        assertThat(savedTalk.getViews(), equalTo(1600L));
        assertThat(savedTalk.getLink(), equalTo(URI.create("https://ted.com/talks/dave_matthews_dive_in")));
    }

    @Test
    void invalidObjectRejected() {
        TedTalk talk = new TedTalk("\"Dive In\"", "Dave Matthews", "October 2021", -1600L, -49L, URI.create("https://ted.com/talks/dave_matthews_dive_in"));
        tedTalkRepository.save(talk);
        ConstraintViolationException e = assertThrows(ConstraintViolationException.class, entityManager::flush);
        assertThat(e.getMessage(), containsString("likes"));
        assertThat(e.getMessage(), containsString("views"));
    }
}