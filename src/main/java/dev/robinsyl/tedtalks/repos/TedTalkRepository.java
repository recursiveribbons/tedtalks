package dev.robinsyl.tedtalks.repos;

import dev.robinsyl.tedtalks.entities.TedTalk;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TedTalkRepository extends JpaRepository<TedTalk, Long> {

}
