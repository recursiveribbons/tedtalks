package dev.robinsyl.tedtalks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TedTalksApplication {

    public static void main(String[] args) {
        SpringApplication.run(TedTalksApplication.class, args);
    }

}
