package dev.robinsyl.tedtalks.services;

import dev.robinsyl.tedtalks.entities.TedTalk;
import dev.robinsyl.tedtalks.repos.TedTalkRepository;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Supplier;

import static java.util.Optional.ofNullable;

@Service
public class TedTalkService {

    private final TedTalkRepository repository;

    public TedTalkService(TedTalkRepository repository) {
        this.repository = repository;
    }

    public Optional<TedTalk> getTedTalkById(Long id) {
        return repository.findById(id);
    }

    public List<TedTalk> getTedTalksPaged(int page, int pageSize, boolean ascending, String sortBy) {
        Sort.Direction direction = ascending ? Sort.Direction.ASC : Sort.Direction.DESC;
        Pageable pageable = PageRequest.of(page, pageSize, Sort.by(new Sort.Order(direction, sortBy)));
        return repository.findAll(pageable).getContent();
    }

    public List<TedTalk> searchTedTalks(String author, String title, Long views, Long likes) {
        TedTalk talk = new TedTalk();
        ofNullable(author).ifPresent(talk::setAuthor);
        ofNullable(title).ifPresent(talk::setTitle);
        ofNullable(views).ifPresent(talk::setViews);
        ofNullable(likes).ifPresent(talk::setLikes);
        ExampleMatcher matcher = ExampleMatcher.matching()
                .withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING)
                .withIgnoreCase();
        Example<TedTalk> example = Example.of(talk, matcher);
        return repository.findAll(example);
    }

    public TedTalk saveTedTalk(TedTalk tedTalk) {
        return repository.save(tedTalk);
    }

    public TedTalk editTedTalk(Long id, TedTalk tedTalk) {
        TedTalk savedTalk = repository.findById(id).orElseThrow(IllegalArgumentException::new);
        setIfNotNull(tedTalk::getTitle, savedTalk::setTitle);
        setIfNotNull(tedTalk::getAuthor, savedTalk::setTitle);
        setIfNotNull(tedTalk::getDate, savedTalk::setDate);
        setIfNotNull(tedTalk::getViews, savedTalk::setViews);
        setIfNotNull(tedTalk::getLikes, savedTalk::setLikes);
        setIfNotNull(tedTalk::getLink, savedTalk::setLink);
        return repository.save(savedTalk);
    }

    private <T> void setIfNotNull(Supplier<T> source, Consumer<T> destination) {
        T value = source.get();
        if (value != null) {
            destination.accept(value);
        }
    }

    public void deleteTedTalk(Long id) {
        if (getTedTalkById(id).isEmpty()) {
            throw new IllegalArgumentException("Ted talk not found");
        }
        repository.deleteById(id);
    }
}
