package dev.robinsyl.tedtalks.config;

import dev.robinsyl.tedtalks.entities.TedTalk;
import dev.robinsyl.tedtalks.repos.TedTalkRepository;
import dev.robinsyl.tedtalks.rest.TedTalkController;
import jakarta.annotation.PostConstruct;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URI;
import java.util.logging.Logger;

@Component
@AutoConfigureBefore(TedTalkController.class)
public class InitialDataLoader {

    private static final Logger log = Logger.getLogger(InitialDataLoader.class.getName());
    private static final String[] HEADERS = new String[]{"title", "author", "date", "views", "likes", "link"};

    private final TedTalkRepository repository;
    private final Resource resourceFile;

    public InitialDataLoader(TedTalkRepository repository, @Value("classpath:data.csv") Resource resourceFile) {
        this.repository = repository;
        this.resourceFile = resourceFile;
    }

    @PostConstruct
    public void readFiles() {
        try (Reader in = new InputStreamReader(resourceFile.getInputStream())) {
            Iterable<CSVRecord> records = CSVFormat.Builder.create()
                    .setHeader(HEADERS)
                    .setSkipHeaderRecord(true)
                    .build()
                    .parse(in);
            int total = 0;
            for (CSVRecord csvRecord : records) {
                insertRecord(csvRecord);
                total++;
            }
            final int count = total;
            log.info(() -> String.format("Initialized %d entries, %s successful", count, repository.count()));
        } catch (IOException e) {
            throw new BeanInitializationException("Unable to initialize data from CSV", e);
        }
    }

    private void insertRecord(CSVRecord csvRecord) {
        try {
            TedTalk tedTalk = new TedTalk();
            tedTalk.setTitle(csvRecord.get("title"));
            tedTalk.setAuthor(csvRecord.get("author"));
            tedTalk.setDate(csvRecord.get("date"));
            tedTalk.setViews(Long.valueOf(csvRecord.get("views")));
            tedTalk.setLikes(Long.valueOf(csvRecord.get("likes")));
            tedTalk.setLink(new URI(csvRecord.get("link")));
            repository.save(tedTalk);
        } catch (Exception e) {
            log.warning(() -> String.format("Unable to insert entry with title <%s>. Reason: %s", csvRecord.get("title"), e.getMessage()));
        }
    }
}
