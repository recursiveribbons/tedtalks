package dev.robinsyl.tedtalks.rest;

import dev.robinsyl.tedtalks.entities.TedTalk;
import dev.robinsyl.tedtalks.services.TedTalkService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/tedtalks")
public class TedTalkController {

    private final TedTalkService service;

    public TedTalkController(TedTalkService service) {
        this.service = service;
    }

    @Operation(summary = "List all Ted Talks")
    @ApiResponse(responseCode = "200", description = "OK")
    @GetMapping
    public List<TedTalk> getTedTalks(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "50") int pageSize, @RequestParam(defaultValue = "date") String sortBy, @RequestParam(defaultValue = "ASC") String sortDirection) {
        boolean direction = parseDirection(sortDirection);
        return service.getTedTalksPaged(page, pageSize, direction, sortBy);
    }

    @Operation(summary = "Get a Ted Talk by its id")
    @ApiResponse(responseCode = "200", description = "OK")
    @ApiResponse(responseCode = "404", description = "Not found")
    @GetMapping("{id}")
    public TedTalk getTedTalk(@Parameter(description = "ID of the Ted Talk") @PathVariable Long id) {
        return service.getTedTalkById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @Operation(summary = "Search for a Ted Talk")
    @ApiResponse(responseCode = "200", description = "OK")
    @GetMapping("search")
    public List<TedTalk> searchTalks(@Parameter(description = "Author of the Ted Talk") @RequestParam(required = false) String author, @Parameter(description = "Title of the Ted Talk") @RequestParam(required = false) String title, @Parameter(description = "View count of the Ted Talk") @RequestParam(required = false) Long views, @Parameter(description = "Like count of the Ted Talk") @RequestParam(required = false) Long likes) {
        return service.searchTedTalks(author, title, views, likes);
    }

    @Operation(summary = "Create a Ted Talk")
    @ApiResponse(responseCode = "201", description = "Created")
    @PostMapping
    public ResponseEntity<TedTalk> createTedTalk(@Valid @RequestBody TedTalk tedTalk) {
        TedTalk savedTalk = service.saveTedTalk(tedTalk);
        return ResponseEntity.created(URI.create("/tedtalk/" + savedTalk.getId())).body(savedTalk);
    }

    @Operation(summary = "Edit a Ted Talk")
    @ApiResponse(responseCode = "200", description = "OK")
    @PatchMapping("{id}")
    public TedTalk patchTedTalk(@Parameter(description = "ID of the Ted Talk") @PathVariable Long id, @RequestBody TedTalk tedTalk) {
        return service.editTedTalk(id, tedTalk);
    }

    @Operation(summary = "Replace a Ted Talk")
    @ApiResponse(responseCode = "200", description = "OK")
    @PutMapping("{id}")
    public TedTalk replaceTedTalk(@Parameter(description = "ID of the Ted Talk") @PathVariable Long id, @Valid @RequestBody TedTalk tedTalk) {
        // This is the same as patch but with validation through @Valid
        return service.editTedTalk(id, tedTalk);
    }

    @Operation(summary = "Delete a Ted Talk")
    @ApiResponse(responseCode = "204", description = "No content")
    @ApiResponse(responseCode = "404", description = "Not found")
    @DeleteMapping("{id}")
    public ResponseEntity<Object> deleteTedTalk(@Parameter(description = "ID of the Ted Talk") @PathVariable Long id) {
        try {
            service.deleteTedTalk(id);
            return ResponseEntity.noContent().build();
        } catch (IllegalArgumentException e) {
            return ResponseEntity.notFound().build();
        }
    }

    private boolean parseDirection(String direction) {
        return switch (direction.toUpperCase()) {
            case "ASC" -> true;
            case "DESC" -> false;
            default -> throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        };
    }
}
