package dev.robinsyl.tedtalks.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PositiveOrZero;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.net.URI;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TedTalk {
    @Id
    @GeneratedValue
    private Long id;
    @NotBlank
    private String title;
    @NotBlank
    private String author;
    @NotBlank
    private String date;
    @PositiveOrZero
    private Long views;
    @PositiveOrZero
    private Long likes;
    @NotNull
    private URI link;

    public TedTalk(String title, String author, String date, Long views, Long likes, URI link) {
        this(null, title, author, date, views, likes, link);
    }
}
